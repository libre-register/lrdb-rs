use crate::sql_helper::*;
use argon2::{
    password_hash::{PasswordHash, PasswordHasher, PasswordVerifier, SaltString},
    Argon2
};

pub struct LogonUser {
    pub uuid: String,
    pub username: String,
    pub hashed_password: String,
    pub permissions: u8,
}

impl LogonUser {
    pub fn new(username: String, password: String, permissions: u8) -> LogonUser {


		// Generates Argon2
		let argon2 = Argon2::default();

        // Hashes the password
        let hash = LogonUser::hash_pass(&password);

        // Verify Password Against PHC string
        let parsed_hash = PasswordHash::new(&hash).unwrap();

        // Panics if it doesn't
        assert!(argon2.verify_password(password.as_bytes(), &parsed_hash).is_ok());

        // Creates and returns a logon user
        LogonUser {
            uuid: gen_id(),
            username: username,
            hashed_password: hash.to_string(),
            permissions: permissions,
        }
    }

    /// Creates a logon user from a pre existing  hash and salt.
    pub fn new_from_hash(
        uuid: String,
        username: String,
        password: String,
        permissions: u8,
    ) -> LogonUser {
        // Creates and returns a logon user
        LogonUser {
            uuid: uuid,
            username: username,
            hashed_password: password,
            permissions: permissions,
        }
    }
    /// Creates a logon user from a pre existing  hash and salt.
    pub fn new_from_hash_str(
        uuid: &str,
        username: &str,
        password: &str,
        permissions: u8,
    ) -> LogonUser {
        // Creates and returns a logon user
        LogonUser {
            uuid: uuid.to_string(),
            username: username.to_string(),
            hashed_password: password.to_string(),
            permissions: permissions,
        }
    }
	// Hashes a password
	pub fn hash_pass(
    	pass: &str
	) -> String {
		// Generates a salt.
        let salt = SaltString::generate(&mut rand_core::OsRng);
		// Generates Argon2
		let argon2 = Argon2::default();

        // Hashes the password
        let hash = argon2.hash_password_simple(
	        pass.as_bytes(),
	        salt.as_ref(), 
        ).unwrap().to_string();

        hash
	}
	
    // Verifies the password
    pub fn verify(&self, pass_attempt: &str) -> bool {
		// Generates Argon2
		let argon2 = Argon2::default();

        // Verify Password Against PHC string
        let parsed_hash = PasswordHash::new(
	        &self.hashed_password
        ).unwrap();

        argon2.verify_password(pass_attempt.as_bytes(), &parsed_hash).is_ok()
    }

    // Updates this to a database
    pub fn update_to_database(&self, path: &str) {
        sql_insert(
            path,
            // Starts formatting
            format!(
                include_str!("../data/sql/insert_logon_user.sql"),
                username = sql_str(self.username.as_str()),
                passwd = sql_str(self.hashed_password.as_str()),
                permissions = self.permissions,
                uuid = sql_str(self.uuid.as_str()),
            ),
        );
    }

    // Converts the permissions to a human readable string.
    pub fn permissions_to_string (&self) -> &str {
		match self.permissions {
			1 => "Admin",
			2 => "Teacher",
			3 => "Supply",
			4 => "Student",
			_ => "inv@l1d Per#isi0n$"
		}
    }
}
