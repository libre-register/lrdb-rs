pub enum ContactPlatform {
    Email,
    Tel,
    Discord,
    Matrix,
    Skype,
    Aim,
    Jabber,
    Icq,
    Groupwise,
    Gadugadu,
    Unknown,
}

impl ContactPlatform {
    // Converts this to an integer
    pub fn to_int(self: ContactPlatform) -> u8 {
        match self {
            ContactPlatform::Email => 0,
            ContactPlatform::Tel => 1,
            ContactPlatform::Discord => 2,
            ContactPlatform::Matrix => 3,
            ContactPlatform::Skype => 4,
            ContactPlatform::Aim => 5,
            ContactPlatform::Jabber => 6,
            ContactPlatform::Icq => 7,
            ContactPlatform::Groupwise => 8,
            ContactPlatform::Gadugadu => 9,
            ContactPlatform::Unknown => 10,
        }
    }
    // Converts this to an integer
    pub fn from_int(int : u8) -> ContactPlatform {
        match int {
            0 	=> ContactPlatform::Email,
            1 	=> ContactPlatform::Tel,
            2 	=> ContactPlatform::Discord,
            3 	=> ContactPlatform::Matrix,
            4 	=> ContactPlatform::Skype,
            5 	=> ContactPlatform::Aim,
            6 	=> ContactPlatform::Jabber,
            7 	=> ContactPlatform::Icq,
            8 	=> ContactPlatform::Groupwise,
            9 	=> ContactPlatform::Gadugadu,
            _	=> ContactPlatform::Unknown,
        }
    }
    // Creates a new ContactPlatform from String, as found in VCard.
    pub fn new_from_string(string: &str) -> ContactPlatform {
        match string {
            "EMAIL" => ContactPlatform::Email,
            "TEL" => ContactPlatform::Tel,
            "X-DISCORD" => ContactPlatform::Discord,
            "X-MATRIX" => ContactPlatform::Matrix,
            "X-SKYPE" => ContactPlatform::Skype,
            "X-AIM" => ContactPlatform::Aim,
            "X-JABBER" => ContactPlatform::Jabber,
            "X-ICQ" => ContactPlatform::Icq,
            "X-GROUPWISE" => ContactPlatform::Groupwise,
            "X-GADUGADU" => ContactPlatform::Gadugadu,
            _ => ContactPlatform::Unknown,
        }
    }
}

impl std::fmt::Display for ContactPlatform {
    // Formats this
    fn fmt(self: &ContactPlatform, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                ContactPlatform::Email => "EMAIL",
                ContactPlatform::Tel => "TEL",
                ContactPlatform::Discord => "X-DISCORD",
                ContactPlatform::Matrix => "X-MATRIX",
                ContactPlatform::Skype => "X-SKYPE",
                ContactPlatform::Aim => "X-AIM",
                ContactPlatform::Jabber => "X-JABBER",
                ContactPlatform::Icq => "X-ICQ",
                ContactPlatform::Groupwise => "X-GROUPWISE",
                ContactPlatform::Gadugadu => "X-GADUGADU",
                ContactPlatform::Unknown => "X-UNKNOWN",
            }
        )
    }
}

// Equal functions
impl std::cmp::PartialEq for ContactPlatform {
    // Equal Function
    fn eq(&self, other: &Self) -> bool {
        // Return value
        self.to_string() == other.to_string()
    }
}
