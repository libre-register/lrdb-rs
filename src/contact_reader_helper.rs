use contack::Contact;
use log::*;

pub(crate) fn sql_to_string(val : &sqlite::Value) -> String {

	trace!("SQL TO STRING");
	match val.as_string() {
		None => "".to_string(),
		Some(val) => val.to_string(),
	}
}

// Creates a new Contact
pub(crate) fn restore_contact(row : &[sqlite::Value]) -> Contact {

	trace!("Restoring Contact");
	// Creates the Contact
	let mut contact = Contact::new(
		contack::name::Name::new(
			Some(sql_to_string(&row[12])), // First Name
			Some(sql_to_string(&row[13])), // Middle Name
			Some(sql_to_string(&row[14])), // Family Name
			Some(sql_to_string(&row[11])), // Prefixes
			None
		)
	);

	// Sets the UID
	contact.uid = sql_to_string(&row[0]);

	// Returns the Contact
	contact
}

// Restores the addresses
pub(crate) fn restore_addresses(row : &[sqlite::Value], contact : &mut Contact) {
	trace!("Restoring Addresses");
	contact.work_address = Some(restore_address(row, 6));
	contact.home_address = Some(restore_address(row, 1));
}

// Restores a single address
fn restore_address(
	row : &[sqlite::Value],
	start : usize,
) -> contack::address::Address {

	trace!("Restoring Address");
	// Creates the address
	contack::address::Address::new (
		Some(sql_to_string(&row[start+4])), // Street
		Some(sql_to_string(&row[start+3])), // Locality
		Some(sql_to_string(&row[start+2])), // Region
		Some(sql_to_string(&row[start+0])), // Code
		Some(sql_to_string(&row[start+1])), // Country
	)
}

// Restores the org
pub(crate) fn restore_org (row: &[sqlite::Value], contact: &mut Contact) {
	trace!("Restoring Org");
	contact.org = Some (
		contack::org::Org::new (
			sql_to_string(&row[19]),
			sql_to_string(&row[17]),
			sql_to_string(&row[18])
		)
	)
}

// Restores the birthday
pub(crate) fn restore_birthday(
	row: &[sqlite::Value], // Row
	contact: &mut Contact // Contact
) -> Result<(), Box<dyn std::error::Error>> /* Errors */ {
	trace!("Restoring the Birthday");
	// Gets the birthday
	let bdaystr = sql_to_string(&row[15]);

	if !bdaystr.is_empty() {
		// Sets the birthday
		contact.bday = Some(
			contack::date_time::DateTime::new (
				bdaystr.get(0..4).ok_or("Invalid SQL Data When parsing birthday.")?.parse::<i64>()?,
				bdaystr.get(4..6).ok_or("Invalid SQL Data When parsing birthday.")?.parse::<u8>()?,
				bdaystr.get(6..8).ok_or("Invalid SQL Data When parsing birthday.")?.parse::<u8>()?,
				0,
				0,
				0
			)
		);
	}
	// Returns an Ok, if all goes to plan
	Ok(())
}

// Restores the contact information
pub(crate) fn restore_contact_information (
	cursor: &mut sqlite::Cursor,
	contact: &mut Contact,
) -> Result<(), Box<dyn std::error::Error>> /* Errors are Erratic */ {
	trace!("Restoring Contact Information");
	
	// Loops through all the contacts
	while let Some(row) = cursor.next()? {
		
		// Creates the Contact Information
		let mut ci = contack::contact_information::ContactInformation::new(
			sql_to_string(&row[3]),
			contack::contact_platform::ContactPlatform::from_int(
				row[4]
					.as_integer()
					.ok_or(
						"Invalid SQL DataTypes or Invalid NULL \
						when setting pref"
					)?
					as u8
			)
		);

		// Sets the PID
		ci.pid = row[0].as_string().ok_or(
			"Invalid SQL Datatype or Invalid NULL \
			when setting PID")?.to_string();

		// Sets the pref
		ci.pref = (row[2].as_integer().ok_or(
			"INVALID SQL Datatype or Invalid NULL \
			when setting PREF"
		)?) as u8;

		// Sets the type
		ci.typ = match row[5].as_integer().ok_or(
			"INVALID SQL Datatype or Invalid NULL \
			when setting Type"
		)? {
			0 => Some(contack::contact_information::Type::Home),
			1 => Some(contack::contact_information::Type::Work),
			_ => None
		};

		contact.contact_information.push(ci);
	}

	Ok(())
}
