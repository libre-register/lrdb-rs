// Runs a Given SQL Statement To Insert Or Update. A Help Function
pub(crate) fn sql_insert(path: &str, query: String) {
    let connection = sqlite::open(path).unwrap();
    match connection.execute(&query) {
        Err(err) => println!(
	        "{}\n Error in statement\n {:#?} \n \
	        Statement is as follows:\n {} \n", path, err.message, &query),
        Ok(()) => {}
    };
}

pub(crate) fn gen_id() -> String {
    // Generates a UID from the UUID Value
    uuid::Uuid::new_v4().to_string()
}
// Turns a string into its sql representation.
pub(crate) fn sql_string(string: String) -> String {
    if string.is_empty() {
		return "NULL".to_string();
    } else {
        let string = format!("\'{}\'", string.replace("'", "''"));
		return string;
    }
}

// Turns an &str into its sql representation.
pub(crate) fn sql_str(string: &str) -> String {
    if string.is_empty()  {
		return "NULL".to_string();
    } else {
        let string = format!("\'{}\'", string.replace("'", "''"));
		return string.to_string();
    }
}

// Turns an Option<String> into an sql string
pub(crate) fn sql_op_string(
	string: &Option<String>
) -> std::string::String {
	match op_string_to_op_str(string) {
		None => "NULL".to_string(),
		Some(string) => sql_str(string)
	}
}

// Turns a Option<String> to Option<&str>
pub(crate) fn op_string_to_op_str (
	string: &Option<String>
) -> Option<&str> {
	string.as_ref().map_or(None, |x| Some(&**x))
}
