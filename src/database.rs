use crate::contact_writer_helper::*;
use crate::contact_reader_helper::*;
use crate::sql_helper::*;
use contack::Contact;
use crate::logon_user::LogonUser;

pub struct Database {
	file_path: String,
}

impl Database {
	// Creates a new Database
	#[allow(dead_code)]
	pub fn new_from_existing(file_path: &str) -> Database {
		let db = Database {
			file_path: file_path.to_string(),
		};
		return db;
	}

	#[allow(dead_code)]
	pub fn new(file_path: &str, name: String) -> Database {
		match std::fs::remove_file(file_path.to_string()) {
			_ => {}
		}

		// Creates a database
		let db = Database::new_from_existing(file_path);
		// Populates the name into it.
		sql_insert(
			db.file_path.as_str(),
			format!(
				include_str!("../data/sql/setup_tables.sql"),
				sql_string(name)
			),
		);
		db
	}

	#[allow(dead_code)]
	// Adds a contact to the database
	pub fn add_contact(&mut self, contact: &mut Contact) {
		sql_insert(
			self.file_path.as_str(), // The Database
			format!(
				include_str!("../data/sql/add_contact.sql"),
				&sql_str(&contact.uid)
			),
		);
		self.update_contact(contact);
	}

	// Updates a contact into a database
	pub fn update_contact(&mut self, contact: &mut Contact) {
		update_name(self.file_path.as_str(), contact);
		update_addresses(self.file_path.as_str(), contact);
		update_birthday(self.file_path.as_str(), contact);
		update_title(self.file_path.as_str(), contact);
		update_role(self.file_path.as_str(), contact);
		update_orgs(self.file_path.as_str(), contact);
		update_contact_information(self.file_path.as_str(), contact);
	}

	// Adds or updates a logon user to the database
	#[allow(dead_code)]
	pub fn update_logon(&mut self, logon: &crate::logon_user::LogonUser) {
		logon.update_to_database(self.file_path.as_str());
	}

	// Gets all the logon users from a database
	#[allow(dead_code)]
	pub fn restore_all_logon_users(&mut self) -> Vec<crate::logon_user::LogonUser> {
		let connection = sqlite::open(self.file_path.as_str()).unwrap();

		let mut cursor = connection
			.prepare(
				"SELECT * FROM LogonUsers ORDER BY
				permissions, username")
			.unwrap()
			.cursor();
		cursor.bind(&[]).unwrap();

		let mut users = std::vec::Vec::<crate::logon_user::LogonUser>::new();

		while let Some(row) = cursor.next().unwrap() {
			users.push(crate::logon_user::LogonUser::new_from_hash(
				row[0].as_string().unwrap().to_string(),
				row[1].as_string().unwrap().to_string(),
				row[2].as_string().unwrap().to_string(),
				row[3].as_integer().unwrap() as u8,
			))
		}
		users
	}

	// Gets all the contacts from a database
	#[allow(dead_code)]
	pub fn restore_all_contacts(
		&mut self
	) -> Result<Vec<Contact>, Box::<dyn std::error::Error>> {
		let mut contacts = Vec::<Contact>::new();
		let connection = sqlite::open(self.file_path.as_str()).unwrap();
		{
			let mut cursor = connection
				.prepare("SELECT * FROM Contacts ORDER BY family, given, birthdate;")
				.unwrap()
				.cursor();
			cursor.bind(&[]).unwrap();


			while let Some(row) = cursor.next().unwrap() {
				let mut contact = restore_contact(row);
				restore_addresses(
					row,
					&mut contact,
				);
				restore_org(
					row,
					&mut contact,
				);
				restore_birthday(
					row,
					&mut contact,
				)?;

				let mut ci_cursor = connection
					.prepare(
						"SELECT * FROM ContactInfo \
						WHERE uuid = ? \
						ORDER BY uuid ASC, \
							platform ASC, \
							prefered DESC, \
							type ASC, \
							str DESC, \
							id ASC;")
					.unwrap()
					.cursor();
				ci_cursor.bind(
					&[	sqlite::Value::String(
							// Dammit!
							// Although I think this is neccasary
							contact.uid.clone()
						)
					]
				).unwrap();
				restore_contact_information(
					&mut ci_cursor,
					&mut contact
				)?;

				// Adds this contact to the list
				contacts.push (
					contact
				);
			}
		}
		Ok(contacts)
	}
	
	#[allow(dead_code)]
	pub fn delete_contact(&self, contact : &mut Contact) {
		sql_insert (
			&self.file_path,
			format! (
				"DELETE FROM Contacts Where uuid = {}",
				sql_str(&contact.uid)
			)
		);
	}
	
	#[allow(dead_code)]
	pub fn delete_logon_user(&self, logon : &LogonUser) {
		sql_insert (
			&self.file_path,
			format! (
				"DELETE FROM LogonUsers Where uuid = {}",
				sql_str(&logon.uuid)
			)
		);
	}

	#[allow(dead_code)]
	pub fn change_name(&self, name : &str) {
		sql_insert(
    		&self.file_path,
			format!(
				"REPLACE INTO Metadata \
				(id, value) \
				VALUES
				('name', {})",
				sql_str(name)
			)
		)
	}

	#[allow (dead_code)]
	pub fn get_name(&self) -> Result<String, Box<dyn std::error::Error>> {
		// Creates a connection and makes a statement
		let connection = sqlite::open(&self.file_path)?;
		let mut cursor = connection.prepare (
			"SELECT value FROM Metadata WHERE id=\"name\""
		).unwrap().cursor();

		// Binds the statement
		cursor.bind(&[]).unwrap();

		// Selects the row
		let val = cursor.next()?;

		match val {
    		Some(val) => match val.get(0) {
        		Some(val) => Ok(
            		match val.as_string() {
						Some(val) => val.to_string(),
						None => return Err(Box::from("Error, Name not found in SQL")),
                	},
        		),
        		None => Err(Box::from("Error, Name not found in SQL")),
    		},
    		None => Err(Box::from("Error, Name not found in SQL"))
		}
	}

	#[allow(dead_code)]
	pub fn attempt_login(
    	&self, pass : &str, username : &str
	) -> Result<LogonUser, &'static str> {
		// Creates a connection and makes a statement
		let connection = sqlite::open(&self.file_path).unwrap();
		let mut cursor = connection.prepare (
    		format!(
    			"SELECT * FROM LogonUsers WHERE username={}",
    			sql_str(username)
    		)
		).unwrap().cursor();

		// Binds the statement
		cursor.bind(&[]).unwrap();

		// Starts the loop
		while let Some(row) = cursor.next().unwrap() {
    		// Creates a logon user
			let user = crate::logon_user::LogonUser::new_from_hash(
				row[0].as_string().unwrap().to_string(),
				row[1].as_string().unwrap().to_string(),
				row[2].as_string().unwrap().to_string(),
				row[3].as_integer().unwrap() as u8,
			);

			// Validates it
			match user.verify(pass) {
				true => return Ok(user),
				false => return Err("Invalid Password")
			}
		}
		Err("Invalid Username")
	}
}
