use crate::sql_helper::*;
use contack::Contact;

// Updates a contact's name into an sql database
pub(crate) fn update_name(path: &str, contact: &mut Contact) {
	let contact_id = sql_str(&contact.uid);
	// Runs the sql update statement.
	sql_insert(
		path,
		// Starts Formatting.
		format!(
			include_str!("../data/sql/update_name.sql"),
			sql_op_string(&contact.name.prefixes),
			sql_op_string(&contact.name.given),
			sql_op_string(&contact.name.additional),
			sql_op_string(&contact.name.family),
			contact_id,
		),
	);
}
pub(crate) fn update_address(
	path: &str,
	adr: &contack::address::Address,
	typ : &str,
	contact_id : &str,
) {
	// Runs the sql update statement.
	sql_insert(
		path,
		// Starts Formatting.
		format! (
			include_str!(
				"../data/sql/update_address.sql"
			),
			// Converts all of these to strings to be Formatted
			sql_op_string(&adr.code),
			sql_op_string(&adr.country),
			sql_op_string(&adr.region),
			sql_op_string(&adr.locality),
			sql_op_string(&adr.street),
			contact_id,
			type = typ,
		),
	);
}
// Updates a contact's addresses into an sql database
pub(crate) fn update_addresses(path: &str, contact: &mut Contact) {
	let contact_id = sql_str(&contact.uid);
	if contact.home_address != None {
		update_address (
			path,
			contact.home_address.as_ref().unwrap(),
			"home",
			&contact_id
		)
	}
	if contact.work_address != None {
		update_address (
			path,
			contact.work_address.as_ref().unwrap(),
			"work",
			&contact_id
		)
	}
}

// Update a contact's birthday into an sql database
pub(crate) fn update_birthday(path : &str, contact : &mut Contact) {
	let contact_id = sql_str(&contact.uid);
	if contact.bday != None {
		let bday = contact.bday.as_ref().unwrap();
		sql_insert(
			path,
			// Starts Formatting
			format!(
				// Update Birthday
				include_str!("../data/sql/update_birthdate.sql"),
				// Formatting
				sql_string(
					format!(
						"{:04}{:02}{:02}",
						bday.year,
						bday.month,
						bday.day,
					),
				),
				contact_id
			)
		)
	}
}

// Update a contact's job title into an sql database
pub(crate) fn update_title(path: &str, contact: &mut Contact) {
	let contact_id = sql_str(&contact.uid);
	// Checks its not none
	if contact.title != None {
		sql_insert (
			path,
			format!(
				include_str!("../data/sql/update_title.sql"),
				sql_op_string(&contact.title),
				// UID
				contact_id
			)
		)
	}
}

// Update a contact's job role into an sql database
pub(crate) fn update_role(path: &str, contact: &mut Contact) {
	let contact_id = sql_str(&contact.uid);
	// Checks its not none
	if contact.role != None {
		sql_insert (
			path,
			format!(
				include_str!("../data/sql/update_role.sql"),
				sql_op_string(&contact.role),
				// UID
				contact_id
			)
		)
	}
}

// Updates a contact's organisation into an sql database
pub(crate) fn update_orgs(path: &str, contact: &Contact) {
	let contact_id = sql_str(&contact.uid);

	if contact.org != None {
		let org = contact.org.as_ref().unwrap();
		sql_insert(
			path,
			format!(
				include_str!("../data/sql/update_org_information.sql"),
				sql_str(&org.org),
				sql_str(&org.unit),
				sql_str(&org.office),
				contact_id
			)
		);
	}
}

// Updates Contact Information
pub(crate) fn update_contact_information(path: &str, contact: &Contact) {
	let contact_id = sql_str(&contact.uid);

	sql_insert(
		path,
		format!(
			"DELETE FROM ContactInfo WHERE uuid={}",
			contact_id
		)
	);
	
	// Loop through the contact information
	for ci in &contact.contact_information {
		sql_insert (
			path,
			format!(
				include_str!(
					"../data/sql/insert_contact_information.sql"
				),
				// Formats the pid
				sql_str(&ci.pid),
				// Uid
				contact_id,
				ci.pref,
				sql_str(&ci.value),
				// Matches up the type
				match ci.typ.as_ref() {
					None => 2,
					Some(typ) => {
						match typ {
							contack::contact_information::Type::Work => 1,
							contack::contact_information::Type::Home => 0,
						}
					}
				},
				ci.platform.to_int(),
			)
		)
	}
}
