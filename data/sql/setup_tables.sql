-- Login Table 
CREATE TABLE LogonUsers (
	uuid		CHAR(36)		PRIMARY KEY		NOT NULL,
	username	VARCHAR							NOT NULL,
	passwd		CHAR(128)						NOT NULL,
	permissions	INT								NOT NULL
);
-- Contact Table
CREATE TABLE Contacts (
	uuid			CHAR(36)	PRIMARY KEY		NOT NULL, -- 0
	-- Home PostBox
	home_code		TEXT, -- 1
	home_country	TEXT, -- 2
	home_region		TEXT, -- 3
	home_locality	TEXT, -- 4
	home_street		TEXT, -- 5

	-- Work PostBox
	work_code		TEXT, -- 6
	work_country	TEXT, -- 7
	work_region		TEXT, -- 8
	work_locality	TEXT, -- 9
	work_street		TEXT, -- 10

	-- Name
	prefixes		TEXT, -- 11
	given			TEXT, -- 12
	additional		TEXT, -- 13
	family			TEXT, -- 14

	-- Birthdate
	birthdate		TEXT, -- YYYYMMDD 15

	-- Work Information
	role			TEXT, -- 16
	org_unit		TEXT, -- 17
	office			TEXT, -- 18
	organisation	TEXT, -- 19
	title			TEXT, -- 20

	-- Image
	image			VARCHAR
);
-- ContactInfo Table
CREATE TABLE ContactInfo (
	id			INT			PRIMARY KEY		NOT NULL, 	-- 0
	uuid		CHAR(36)					NOT NULL, 	-- 1
	prefered	INT							NOT NULL, 	-- 2
	str			TEXT						NOT NULL, 	-- 3
	platform 	INT, 									-- 4
	type		INT 									-- 5
);
-- Metadata table
CREATE TABLE Metadata (
	id		TEXT		PRIMARY KEY		NOT NULL,
	value	VARCHAR						NOT NULL
);
-- Set up Metadata
INSERT INTO Metadata (id, value) VALUES	('name',{});
